/**
 * Originally from https://github.com/jagrosh/EasySQL, ported to Kotlin for this project for my own purposes.
 *
 * @author GreemDev
 */
package volte.lib.db;